# Test Drive Base Snapshot Builder for AIR

This project does the following
1. Builds a new topology instance based on the `topology.dot` file
2. Creates a simulation based on the topology created above
3. Waits 5 mins for that simulation to load (ZTP, etc)
4. Power off / Store the simulation (so it can be cloned/duplicated). It enters a loop to check the status of the store job. After store is complete, job exits
5. Creates a child pipeline to perform a clone/duplicate action on the base snapshot and 

Important things to manage:  
  
- `topology.dot` - This is how you change the topology. 
- `guided-tour.md` - This is the markdown doc that will show up in the AIR UI. It works pretty well to edit it in the gitlab UI - just pull a branch that isn't staging or prod and work on it there.  
- `helper_files/oob-mgmt-server-customizations.sh` - This is a file that will be executed by the base_simulation, and clones on the oob-mgmt-server. Make sure its idempotent.  
- `helper_files/cumulus-ztp` - ZTP script for the simulation. ZTP runs on the VX nodes during the first (base) simulation run.  
- `tests/oob-mgmt-tests.sh` - Script that runs on oob mgmt server in test job. The test drive lab guide steps are impleneted in ad hoc ansible here.

Nothing else should need to be changed or updated (hopefully). 
