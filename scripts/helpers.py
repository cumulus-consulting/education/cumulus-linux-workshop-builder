#!/usr/bin/env python3

import os
import json
import sys
import traceback
import time
from air_sdk import AirApi
from datetime import datetime, timedelta

# call this function to drop an instruction on a node
# ex)
# drop_file_with_file_executor(OOB_NODE, "helper_files/oob-mgmt-server-customizations.sh", "/home/cumulus/customizations.sh", ["bash /home/cumulus/customizations.sh", "rm /home/cumulus/customizations.sh"])
#
# node = An air api simulation_node object
# src_filename = script in your project by name that you want to drop on the node
# target_filename = full path of where you want that script to go on the node
# post_cmd = list of strings/commands to execute after the file is dropped. Execute the file/script at least
def drop_file_with_file_executor(node, src_filename, target_filename, post_cmd):
    data = {}

    fd = open(src_filename, "r")
    if fd.mode == 'r':
        file_str = fd.read()

    data[target_filename] = file_str
    data["post_cmd"] = post_cmd

    # debugging
    print(f'{src_filename} instruction data is:')
    print(data)

    send_file_executor_instruction(node, data)

# don't call this one
def send_file_executor_instruction(node, data):
    try:
        node.create_instructions(data=json.dumps(data), executor='file')
        print(f'created instruction for: {node}')
    except:
        print(f'Error while creating instruction for: {node}')
        raise

# pass in air api object, simlation id string, string of the node name
def get_simulation_node(air, sim_id, nodestr):
    try: # get simulation node
        simulation_node = air.simulation_nodes.list(simulation=sim_id, name=nodestr)
        if len(simulation_node) > 1:
            print (f'More than one simulation node returned by sim_id and name {nodestr}')
            sys.exit(1)
        simulation_node = simulation_node[0]
    except:
        print(f'Unexpected error while getting the simulation-node for {simulation_node}')
        raise

    return simulation_node

def add_permissions(topo_or_simulation):
    with open('email-permissions.txt') as f:
        emails = f.read().splitlines()

    for email in emails:
        print ("Enabling permission for: " + email)
        try:
            topo_or_simulation.add_permission(email, write_ok=True)
        except:
            print("Failed while adding permissions for: " + email)

# pulls out base simulation id from artifact
def get_simulation_id():
    artifact = {}
    with open("data_artifact.json") as json_file:
        artifact = json.load(json_file)
    return str(artifact["sim_id"])

def get_time_delta(hours):
    return str(datetime.now() + timedelta(hours=hours))

# input air sdk job object
def wait_for_job(job):
    while True:
        job.refresh()
        print("Job refreshed:")
        print(job.__dict__)
        if job.state == "COMPLETE":
            print("Store complete!")
            break
        else:
            print("Job not complete. Sleep for 60 seconds")
            time.sleep(60)

#
def get_documentation_url():
    url = ""
    url = os.getenv("CI_PROJECT_URL") + "/-/raw/" + os.getenv("CI_COMMIT_BRANCH") + "/guided-tour.md"
    #print(url)
    return url

#
def get_dot_file_str(rel_path):
    fd = open(rel_path, "r") # import the .dot file into a string
    if fd.mode == 'r':
        dotfile = fd.read()
    return dotfile

# pass in simulation object and write out the id as an artifact
def write_artifact_file(simulation):
    data_artifact = {}
    data_artifact["sim_id"] = simulation.id

    print(json.dumps(data_artifact, indent=4))
    print("Writing artifact file...")
    with open('data_artifact.json', 'w') as outfile:
        json.dump(data_artifact, outfile)
