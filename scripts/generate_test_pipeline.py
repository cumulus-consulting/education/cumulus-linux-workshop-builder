#!/usr/bin/env python3
"""
:)

"""
import os
import json
import sys
from jinja2 import Environment, FileSystemLoader

def get_simulation_id():
    artifact = {}
    with open("data_artifact.json") as json_file:
        artifact = json.load(json_file)
    return str(artifact["sim_id"])

def render_template():
    file_loader = FileSystemLoader('scripts')
    env = Environment(loader=file_loader)
    template = env.get_template('test-pipeline-template.yml.j2')

    sim_id = get_simulation_id()
    parent_pipeline_id = os.getenv('CI_PIPELINE_ID')
    site = os.getenv('CI_PROJECT_NAME')

    return template.render(site=site, parent_pipeline_id=parent_pipeline_id, base_sim_id=sim_id)

def write_to_file(str):
    # write the yml to file - formatting part of template
    outputfile = open("test-pipeline.yml", "w")
    outputfile.write(str)
    outputfile.close()

if __name__ == '__main__':
    write_to_file(render_template())
