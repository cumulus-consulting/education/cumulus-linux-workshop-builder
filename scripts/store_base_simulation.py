#!/usr/bin/env python3
"""
:)

"""
import os
from air_sdk import AirApi
from helpers import *

if __name__ == '__main__':
    if "staging" in os.getenv('CI_COMMIT_REF_NAME'):
        print ("ON staging branch. using air staging")
        AIR = AirApi(api_url='https://staging.air.cumulusnetworks.com/api/', username=os.getenv('AIR_USERNAME'), password=os.getenv('AIR_PASSWORD'))
    else:
        print ("Didn't detect staging branch. Using air prod")
        AIR = AirApi(username=os.getenv('AIR_USERNAME'), password=os.getenv('AIR_PASSWORD'))

    # fetch the simulation object by id from artifact
    SIMULATION = AIR.simulations.get(get_simulation_id())

    # call action to store the simulation
    RESPONSE = SIMULATION.control(action='store')

    # get job by id
    STORE_JOB = AIR.jobs.get(RESPONSE["jobs"][0])

    print("Simulation Storing. Waiting for store to complete")

    wait_for_job(STORE_JOB)

    print("Simulation Stored.")

    SIMULATION.expires = False

    print("Base simulation expiry disabled.")
