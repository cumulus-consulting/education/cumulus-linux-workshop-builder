#!/usr/bin/env python3
"""
:)

"""
import os
import json
import sys
import traceback
from air_sdk import AirApi
from datetime import datetime, timedelta
from helpers import *

def create_topology(air, dotfile_name, org, diagram_url, topo_name):
    dotfile = get_dot_file_str(dotfile_name)

    #debug output
    print("Dotfile dump after reading from file. before create_topology call")
    print(dotfile)
    print("")

    documentation_url = get_documentation_url()

    # todo: proper error handling
    try:
        topology = air.topologies.create(dot=dotfile)
    except:
        print("Unexpected response trying to create a new topology from the .dot file")
        raise

    print("Topology created. dump:")
    print("")
    print(topology.__dict__)
    print("")

    documentation_url = get_documentation_url()
    print("Got documentation url:")
    print(f'documentation= {documentation_url}')

     # todo: proper error handling
    try:
        topology.update(name=topo_name, organization=org, diagram_url=diagram_url, documentation=documentation_url)
    except:
        print("Unexpected response trying to update topology details")
        raise

    #debug output
    print("Topology updated. dump:")
    print("")
    print(topology.__dict__)
    print("")

    return topology

def create_simulation(air, topology, org, diagram_url):
    expires = "true"
    sleep = "false"
    expires_at = get_time_delta(24)
    name = topology.name + ":base-simulation:" + os.getenv('CI_PIPELINE_ID')

    documentation_url = get_documentation_url()
    metadata = '{"power_off": false, "delete": false}'

    # debug output
    print(f'Sim Create Variable Check - Before Create:')
    print(f'topology= {topology.url}')
    print(f'name= {name}')
    print(f'sleep= {sleep}')
    print(f'expires= {expires}')
    print(f'expires_at= {expires_at}')
    print(f'title= {name}')
    print(f'organization= {org}')
    print(f'documentation= {documentation_url}')
    print(f'diagram_url= {diagram_url}')
    print(f'metadata= {metadata}')
    print("")

    # todo: proper error handling
    try:
        simulation = air.simulations.create(topology=topology, name=name, sleep=sleep, expires=expires, diagram_url=diagram_url,
                     documentation=documentation_url, expires_at=expires_at, title=name, organization=org, metadata=metadata)
    except:
        print("Unexpected response trying to create a new simulation from the topology object")
        raise

    #debug output
    print("Simulation create success.")
    print("Simulation object dump:")
    print("")
    print(simulation.__dict__)
    print("")

    #set_permissions(simulation)
    #print("Done setting permissions on simulation")

    return simulation

if __name__ == '__main__':

    #check for staging
    if "staging" in os.getenv('CI_COMMIT_REF_NAME'):
        print ("ON staging branch. Directing API and org to staging")
        AIR = AirApi(api_url='https://staging.air.cumulusnetworks.com/api/', username=os.getenv('AIR_USERNAME'), password=os.getenv('AIR_PASSWORD'))
        ORGANIZATION = "https://staging.air.cumulusnetworks.com/api/v1/organization/70850521-37a2-4ab9-9f76-1b3508c01d36/" # Cumulus Networks Staging
    else:
        print ("Didn't detect staging branch. Using AIR prod")
        AIR = AirApi(username=os.getenv('AIR_USERNAME'), password=os.getenv('AIR_PASSWORD'))
        ORGANIZATION = "https://air.cumulusnetworks.com/api/v1/organization/2185354c-6a8b-4b0a-b3c1-010d7394b74d/" # Cumulus Linux Test Drive

    # URL for picture/diagram that will be in topo & simulation (relative on AIR)
    DIAGRAM_URL = "/images/testdrive_topology.svg"

    # create topology from dot file, set other attributes
    TOPOLOGY = create_topology(AIR, "topology.dot", ORGANIZATION, DIAGRAM_URL, "NVIDIA Cumulus Linux Test Drive")
    
    # create simulation from topology
    SIMULATION = create_simulation(AIR, TOPOLOGY, ORGANIZATION, DIAGRAM_URL)

    # fetch the simulation_node for oob-mgmt-server
    OOB_NODE = get_simulation_node(AIR, SIMULATION.id, "oob-mgmt-server")

    # add extras to oob-server this base topology 
    drop_file_with_file_executor(OOB_NODE, "helper_files/oob-mgmt-server-customizations.sh", "/home/cumulus/customizations.sh", ["bash /home/cumulus/customizations.sh", "rm /home/cumulus/customizations.sh"])

    # Drop in ZTP script on oob-mgmt-server
    drop_file_with_file_executor(OOB_NODE, "helper_files/cumulus-ztp", "/var/www/html/cumulus-ztp", ["chmod 755 /var/www/html/cumulus-ztp"])
   
    # start the simulation
    SIMULATION.start()

    # write out the simulation ID to a file as an artifact available in future jobs
    write_artifact_file(SIMULATION)

    print("Simulation started!")
    # print more output and info here
