#!/usr/bin/env python

import json
import sys
import os
from air_sdk import AirApi
from jinja2 import Environment, FileSystemLoader
from helpers import *

# function for dropping gitlab runner registration
def drop_runner_reg_instruction(node, file_str):
    #build oob-mgmt-server instructions
    data = {}
    data["/home/cumulus/runner-script.sh"] = file_str
    data["post_cmd"] = ["sh /home/cumulus/runner-script.sh"]

    # debugging
    print(f'{file_str} instruction data is:')
    print(f'{data} data dict we are about to send:')

    send_file_executor_instruction(node, data)

if __name__ == '__main__':
    #check for staging
    if "staging" in os.getenv('CI_COMMIT_REF_NAME'):
        print ("ON staging branch. using air staging")
        AIR = AirApi(api_url='https://staging.air.cumulusnetworks.com/api/', username=os.getenv('AIR_USERNAME'), password=os.getenv('AIR_PASSWORD'))
    else:
        print ("Didn't detect staging branch. Using air prod")
        AIR = AirApi(username=os.getenv('AIR_USERNAME'), password=os.getenv('AIR_PASSWORD'))

    # pull in env vars, set from global variables: in test-pipeline-template
    SITE = os.getenv('SITE')
    PARENT_PIPELINE_ID = os.getenv('PARENT_PIPELINE_ID')
 
    #
    print("jbetz test:")
    print("SITE: " + SITE)
    print("PARENT_PIPELINE_ID: " + PARENT_PIPELINE_ID)

    # get new oob-node from sim_id in artifact of this pipeline (the tested/cloned sim)
    OOB_NODE = get_simulation_node(AIR, get_simulation_id(), "oob-mgmt-server")

    #load template
    file_loader = FileSystemLoader('scripts')
    env = Environment(loader=file_loader)
    template = env.get_template('runner-reg-instructions.sh.j2')

    #render the template/script into a string
    OOB_REG_SCRIPT = template.render(site=SITE, parent_pipeline_id=PARENT_PIPELINE_ID, simulation_node_name="oob-mgmt-server", ci_server_url=os.getenv('CI_SERVER_URL'), runner_reg_token=os.getenv('RUNNER_REG_TOKEN'))

    #render template and send instruction for runner register
    drop_runner_reg_instruction(OOB_NODE, OOB_REG_SCRIPT)

    print ("Done")
