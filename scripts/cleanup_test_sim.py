#!/usr/bin/env python

import os
import sys
from air_sdk import AirApi
from datetime import datetime, timedelta
from helpers import *

if __name__ == '__main__':
    #check for staging
    if "staging" in os.getenv('CI_COMMIT_REF_NAME'):
        print ("ON staging branch. using air staging")
        AIR = AirApi(api_url='https://staging.air.cumulusnetworks.com/api/', username=os.getenv('AIR_USERNAME'), password=os.getenv('AIR_PASSWORD'))
    else:
        print ("Didn't detect staging branch. Using air prod")
        AIR = AirApi(username=os.getenv('AIR_USERNAME'), password=os.getenv('AIR_PASSWORD'))

    TESTED_SIMULATION = AIR.simulations.get(get_simulation_id())

    # Uncomment if we want to unconditionally dispose of the test sim
    #TESTED_SIMULATION.delete()

    # Uncomment if we want to store and power off the test simulation
    #TESTED_SIMULATION.store()

    print("")
    print("")
    print("Tested Simulation Passed:")
    print("")
    print("Tested Simulation Owner: " + TESTED_SIMULATION.name)
    print("Tested Simulation API URL: " + TESTED_SIMULATION.url)
    print("Tested Simulation ID: " + TESTED_SIMULATION.id)
    print("Base Simulation ID: " + os.getenv('BASE_SIM_ID'))
