#!/usr/bin/env python

import os
import json
import sys
from air_sdk import AirApi
from datetime import datetime, timedelta
from helpers import *

if __name__ == '__main__':
    #check for staging
    if "staging" in os.getenv('CI_COMMIT_REF_NAME'):
        print ("ON staging branch. using air staging")
        AIR = AirApi(api_url='https://staging.air.cumulusnetworks.com/api/', username=os.getenv('AIR_USERNAME'), password=os.getenv('AIR_PASSWORD'))
    else:
        print ("Didn't detect staging branch. Using air prod")
        AIR = AirApi(username=os.getenv('AIR_USERNAME'), password=os.getenv('AIR_PASSWORD'))

    # get the project name if not passed in
    SITE = os.getenv('SITE')
    BASE_SIM_ID = os.getenv('BASE_SIM_ID')
    # get the child pipeline if parent not passed in
    PARENT_PIPELINE_ID = os.getenv('PARENT_PIPELINE_ID')
    print("jbetz test:")
    print("SITE: " + SITE)
    print("BASE_SIM_ID: " + BASE_SIM_ID)
    print("PARENT_PIPELINE_ID: " + PARENT_PIPELINE_ID)

    TEST_SIMULATION, RESPONSE = AIR.simulations.duplicate(BASE_SIM_ID)

    print("Simulation clone response:")
    print(RESPONSE)
    print("")
    print("Cloned/Test simulation object dump:")
    print(TEST_SIMULATION.__dict__)
    print("")

    ### Next update/set cloned sim details
    # calculate sleep/expires timers for CI run
    EXPIRES_AT = datetime.now() + timedelta(hours=4)

    TEST_SIMULATION.title = SITE + ":CI Test Simulation:" + PARENT_PIPELINE_ID
    TEST_SIMULATION.name = SITE + ":CI Test Simulation:" + PARENT_PIPELINE_ID
    TEST_SIMULATION.expires_at = EXPIRES_AT.isoformat()
    TEST_SIMULATION.sleep = False
    TEST_SIMULATION.expires = True
#    TEST_SIMULATION.documentation = "Base sim ID: " + get_artifact('data_artifact.json')["sim_id"]

    print("Test sim details post update:")
    print(TEST_SIMULATION.__dict__)
    print("")

    write_artifact_file(TEST_SIMULATION)
    print("Wrote out artifact file. Done.")
    print("")
