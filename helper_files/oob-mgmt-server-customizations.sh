#!/bin/bash
#
# This runs as root. Users use the system as cumulus
# mind your chmods and chowns for things you want user cumulus to use
#
# clone test drive automation repo for on demand test drives
rm -rf /home/cumulus/Test-Drive-Automation
git clone https://github.com/CumulusNetworks/cumulus-test-drive /home/cumulus/Test-Drive-Automation
chown -R cumulus:cumulus /home/cumulus/Test-Drive-Automation

# add some ansible ssh convenience settings so ad hoc ansible works easily
cat <<EOT > /etc/ansible/ansible.cfg
[defaults]
roles_path = ./roles
host_key_checking = False
pipelining = True
forks = 50
deprecation_warnings = False
jinja2_extensions = jinja2.ext.do
force_handlers = True
callback_whitelist = profile_tasks
retry_files_enabled = False
transport = paramiko
ansible_managed = # Ansible Managed File
interpreter_python = auto_silent

[ssh_connection]
ssh_args = -o ControlMaster=auto -o ControlPersist=60s -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null

EOT
chmod -R 755 /etc/ansible/*

# disable ssh key checking
cat <<EOT > /home/cumulus/.ssh/config
Host *
    StrictHostKeyChecking no
EOT
chown -R cumulus:cumulus /home/cumulus/.ssh

apt update -qy
apt install -qy ntp
