#!/bin/bash

check_state(){
if [ "$?" != "0" ]; then
    echo "ERROR Could not bring up last series of devices, there was an error of some kind!"
    exit 1
fi
}

echo "Test Ansible Ping for all nodes"
ansible spine01:leaf01:leaf02:server01:server02 -m ping
check_state


###Lab 1

# Step 6,7,8
cd /home/cumulus/Test-Drive-Automation
check_state
sudo git pull
check_state
ansible-playbook start-lab.yml
check_state

#Step 10
ansible leaf01 -a 'curl http://192.168.200.1/license.lic'
ansible leaf01 -a 'cl-license -i http://192.168.200.1/license.lic' -b
ansible leaf01 -a 'cat /etc/cumulus/.license'

#Step 11
ansible leaf01 -a 'systemctl restart switchd.service' -b
ansible leaf01 -a 'systemctl status switchd.service' -b 

####Lab2

#step 1
ansible leaf01 -a 'net add loopback lo ip address 10.255.255.1/32'
ansible leaf01 -a 'net commit'

#step2
ansible leaf02 -a 'net add loopback lo ip address 10.255.255.2/32'
ansible leaf02 -a 'net commit'

sleep 3

#step3
ansible leaf01 -a 'net show interface lo'

#step4
ansible leaf02 -a 'net show interface lo'

#step5
ansible leaf01 -a 'net add bond BOND0 bond slaves swp49-50'
ansible leaf01 -a 'net commit'

#step6
ansible leaf02 -a 'net add bond BOND0 bond slaves swp49-50'
ansible leaf02 -a 'net commit'

sleep 3

#step7
ansible leaf01 -a 'net show interface bonds'
ansible leaf01 -a 'net show interface bondmems'
ansible leaf02 -a 'net show interface bonds'
ansible leaf02 -a 'net show interface bondmems'

#step 8
ansible leaf01 -a 'net add bridge bridge vids 10,20'
#step9
ansible leaf01 -a 'net add bridge bridge ports BOND0,swp1'
#step10
ansible leaf01 -a 'net add interface swp1 bridge access 10'
#step11
ansible leaf01 -a 'net commit'

#step 12
ansible leaf02 -a 'net add bridge bridge vids 10,20'
ansible leaf02 -a 'net add bridge bridge ports BOND0,swp2'
ansible leaf02 -a 'net add interface swp2 bridge access 20'
ansible leaf02 -a 'net commit'

sleep 3

#step13
ansible leaf01 -a 'net show bridge vlan'

#step 14
ansible leaf02 -a 'net show bridge vlan'

#step 15
ansible leaf01 -a 'net add vlan 10 ip address 10.0.10.2/24'
#step 16
ansible leaf01 -a 'net add vlan 20 ip address 10.0.20.2/24'
#step 17
ansible leaf01 -a 'net add vlan 10 ip address-virtual 00:00:00:00:1a:10 10.0.10.1/24'
#step 18
ansible leaf01 -a 'net add vlan 20 ip address-virtual 00:00:00:00:1a:20 10.0.20.1/24'
#step 19
ansible leaf01 -a 'net commit'

#step 20
ansible leaf02 -a 'net add vlan 10 ip address 10.0.10.3/24'
ansible leaf02 -a 'net add vlan 20 ip address 10.0.20.3/24'
ansible leaf02 -a 'net add vlan 10 ip address-virtual 00:00:00:00:1a:10 10.0.10.1/24'
ansible leaf02 -a 'net add vlan 20 ip address-virtual 00:00:00:00:1a:20 10.0.20.1/24'
ansible leaf02 -a 'net commit'

sleep 5

#Lab 2 verify

#step21
ansible server01 -a 'ping -c 4 10.0.10.1'

#step22
ansible server01 -a 'ping -c 4 10.0.10.2'

#step23
ansible server01 -a 'ping -c 4 10.0.10.3'

#step24
ansible server01 -a 'ip neighbor show'

#step25
ansible server02 -a 'ping -c 4 10.0.20.1'
ansible server02 -a 'ping -c 4 10.0.20.2'
ansible server02 -a 'ping -c 4 10.0.20.3'
ansible server02 -a 'ip neighbor show'

#step26
ansible server01 -a 'ping -c 4 10.0.20.102'
ansible server02 -a 'ping -c 4 10.0.10.101'

#step27
ansible server01 -a 'traceroute 10.0.20.102'
ansible server02 -a 'traceroute 10.0.10.101'

#step28
ansible leaf01 -a 'net show bridge macs'
ansible leaf02 -a 'net show bridge macs'

#### LAB 3
#step1
ansible-playbook lab3.yml

#step2
ansible spine01 -a 'net add loopback lo ip address 10.255.255.101/32'
ansible spine01 -a 'net commit'

#step3
ansible spine01 -a 'net add bgp autonomous-system 65201'
ansible spine01 -a 'net add bgp bestpath as-path multipath-relax'

#step4
ansible spine01 -a 'net add bgp neighbor swp1 interface remote-as external'
ansible spine01 -a 'net add bgp neighbor swp2 interface remote-as external'

#step5
ansible spine01 -a 'net commit'

#step6
ansible leaf01 -a 'net add bgp autonomous-system 65101'
ansible leaf01 -a 'net add bgp bestpath as-path multipath-relax'
ansible leaf01 -a 'net add bgp neighbor swp51 interface remote-as external'
ansible leaf01 -a 'net commit'

#step7
ansible leaf02 -a 'net add bgp autonomous-system 65102'
ansible leaf02 -a 'net add bgp bestpath as-path multipath-relax'
ansible leaf02 -a 'net add bgp neighbor swp51 interface remote-as external'
ansible leaf02 -a 'net commit'

sleep 5

#step8
ansible spine01 -a 'net show bgp summary'

#step9
ansible leaf01 -a 'net show bgp summary'
ansible leaf02 -a 'net show bgp summary'

#step10
ansible spine01 -a 'net add bgp network 10.255.255.101/32'
ansible spine01 -a 'net commit'

#step11
ansible leaf01 -a 'net add bgp network 10.255.255.1/32'

#step12
ansible leaf01 -a 'net add bgp network 10.0.10.0/24'

#step13
ansible leaf01 -a 'net commit'

#step14
ansible leaf02 -a 'net add bgp network 10.255.255.2/32'
ansible leaf02 -a 'net add bgp network 10.0.20.0/24'
ansible leaf02 -a 'net commit'

sleep 5

#step15
ansible spine01 -a 'net show bgp'

#step 16
ansible server01 -a 'ping -c 4 10.0.20.102'

#step 17
ansible server01 -a 'traceroute 10.0.20.102'

